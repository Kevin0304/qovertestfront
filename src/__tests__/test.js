import React from "react";
import { create } from "react-test-renderer";
import SignIn from "../SignIn";

describe("Feature component", () => {
  test("it matches the snapshot", () => {
    const component = create(<SignIn />);
    expect(component.toJSON()).toMatchSnapshot();
  });
});