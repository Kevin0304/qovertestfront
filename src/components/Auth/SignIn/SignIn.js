import React, { Component } from "react";
import "./SignIn.css";
import checkbox from "../../../assets/svg/checked-no-label.svg";
import SigninBtn from '../SigninBtn'
import AuthInput from "../AuthInput";

class SignIn extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      validEmail: false,
      validPassword: false,
      validForm: false,
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleKeyUp = this.handleKeyUp.bind(this);
  }

  handleSubmit() {
    this.props.submit(this.state.email, this.state.password);
  }

  handleKeyUp(event) {
    if (event.keyCode === 13 && this.state.validForm)
      return this.handleSubmit();
    else return;
  }

  handleChange(event) {
    const name = event.target.name;
    const value = event.target.value;
    this.setState({ [name]: value }, () => {
      this.validateField(name, value);
    });
  }

  validateField(fieldName, value) {
    let validEmail = this.state.validEmail;
    let validPassword = this.state.validPassword;

    switch (fieldName) {
      case "email":
        validEmail = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
        break;
      case "password":
        validPassword = value.length > 0;
        break;
      default:
        break;
    }
    this.setState(
      {
        validEmail: validEmail,
        validPassword: validPassword
      },
      this.validateForm
    );
  }

  validateForm() {
    this.setState({
      validForm: this.state.validEmail && this.state.validPassword
    });
  }

  render() {
    return (
      <div className="Rectangle-Copy">
        <div className="Welcome-at-Qover">Welcome at Qover</div><br></br>
          <AuthInput change={this.handleChange} enterPress={this.handleKeyUp} name="email" label="Email" type="email" value={this.state.email} />
          <br />
          <AuthInput change={this.handleChange} enterPress={this.handleKeyUp} name="password" label="Password" type="password" value={this.state.password} />
          <br></br>
          
          <div className="Password-Remember">
            <img alt="" className="Checked-box" src={checkbox} />
            <span className="Remember-me"> Remember me</span>
            <span className="Forgot-your-password">Forgot your password?</span>
          </div><br></br>
          <SigninBtn click={this.handleSubmit} type="submit" text="Sign in to your account" enable={this.state.validForm}/>
      </div>
    );
  }
}

export default SignIn;
