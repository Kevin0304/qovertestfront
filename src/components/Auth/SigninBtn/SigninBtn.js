import React, { Component } from "react";
import "./SigninBtn.css";

class SigninBtn extends Component {
  render() {
    return (
      <button
        onClick={this.props.click}
        disabled={!this.props.enable}
        className={!this.props.enable ? "Disabled-Signin" : "Signin-Btn"}
      >
        {this.props.text}
      </button>
    );
  }
}

export default SigninBtn;
