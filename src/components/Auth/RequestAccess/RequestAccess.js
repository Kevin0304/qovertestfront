import React, { Component } from 'react';
import './RequestAccess.css';

class RequestAccess extends Component {
  render() {
    return (
      <button className="Rectangle-Copy-2">
          Don't have an account? <u>Ask access</u>
      </button>
    );
  }
}

export default RequestAccess;