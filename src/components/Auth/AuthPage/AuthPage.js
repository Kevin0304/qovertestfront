import React, { Component } from "react";
import logo from "../../../assets/svg/white.svg";
import "./AuthPage.css";
import Header from "../../Header";
import SignIn from "../SignIn/SignIn";
import RequestAccess from "../RequestAccess";
import Footer from "../../Footer/Footer";
import Api from "../../../services/Api";

class AuthPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: false
    };

    this.submit = this.submit.bind(this);
  }

  submit(email, psw) {
    const user = { email: email, password: psw };
    Api.logIn(user)
      .then(res => {
        if (res.data.token) {
          let token = res.data.token;
          // console.log("token", token);
          Api.saveLocalSessionToken(res.data.user, token);
          return this.props.history.push("/quote-request");
        } else {
          return this.setState({ error: true });
        }
      })
      .catch(err => {
        // console.log("error login", err);
        return this.setState({ error: true });
      });
  }

  render() {
    return (
      <div className="Mask">
        <Header />
        <div className="container">
          <img src={logo} className="White" alt="logo" />
          <br />
          {this.state.error ? (
            <p className="error-login">Wrong credentials, please retry!</p>
          ) : null}
          <SignIn submit={this.submit} />
          <br />
          <RequestAccess />
        </div>
        <Footer />
      </div>
    );
  }
}

export default AuthPage;
