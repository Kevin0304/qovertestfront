import React, { Component } from "react";
import "./AuthInput.css";
import { TextField } from "@material-ui/core";

class AuthInput extends Component {
  render() {
    return (
      <TextField
        name={this.props.name}
        label={this.props.label}
        type={this.props.type}
        onKeyDown={this.props.enterPress}
        margin="normal"
        className="Auth-Input"
        onChange={this.props.change}
        value={this.props.value}
        InputLabelProps={{
          shrink: true
        }}
      />
    );
  }
}

export default AuthInput;
