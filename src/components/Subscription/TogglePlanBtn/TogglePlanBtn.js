import React, { Component } from "react";
import "./TogglePlanBtn.css";
import { Switch, Grid } from "@material-ui/core";

class TogglePlanBtn extends Component {
  render() {
    return (
      <div className="TogglePlanBtn">
        <Grid
          align="center"
          justify="center"
          container
          className="toggleBtn"
          alignItems="center"
          spacing={1}
        >
          <Grid
            item
            className={!this.props.annualMode ? "active-label" : "label"}
          >
            PAY MONTHLY
          </Grid>
          <Grid item>
            <Switch
              checked={this.props.checked}
              onChange={this.props.handleChange}
              color="primary"
            />
          </Grid>
          <Grid
            item
            className={this.props.annualMode ? "active-label" : "label"}
          >
            PAY YEARLY
          </Grid>
        </Grid>
      </div>
    );
  }
}

export default TogglePlanBtn;
