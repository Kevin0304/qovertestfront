import React, { Component } from "react";
import "./SubscriptionPage.css";
import SubscriptionCard from "../SubscriptionCard";
import { Grid } from "@material-ui/core";
import TogglePlanBtn from "../TogglePlanBtn";
import background from "../../../assets/svg/background-travel.svg";
import comparisonIcon from "../../../assets/svg/icon-comparison.svg";
import Header from "../../Header/Header";


class SubscriptionPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      universePrice: 0,
      globalPrice: 0,
      annualMode: true,
      globalSelected: false,
      universeSelected: false
    };

    this.selectedPlan = this.selectedPlan.bind(this);
    this.handlePlanChange = this.handlePlanChange.bind(this);
  }

  componentDidMount() {
    const { params } = this.props.match;
    // console.log(params.globalPrice);
    // console.log(params.universePrice);
    this.setState({
      globalPrice: parseFloat(params.globalPrice),
      universePrice: parseFloat(params.universePrice),

    })
  }


  handlePlanChange(event) {
    return this.setState({
      annualMode: event.target.checked
    });
  }

  selectedPlan(plan) {
    return plan === "Global"
      ? this.setState({ globalSelected: true, universeSelected: false })
      : this.setState({ globalSelected: false, universeSelected: true });
  }


  render() {
    return (
      <div className="SubscriptionPage">
        <Header/>
        <img alt="" src={background} className="plane-background" />
        <h1 className="title">Select a plan</h1>
        <TogglePlanBtn
          annualMode={this.state.annualMode}
          checked={this.state.annualMode}
          handleChange={this.handlePlanChange}
        />
        <Grid
          className="card-block"
          spacing={1}
          align="center"
          justify="center"
          container
        >
          <Grid item>
            <SubscriptionCard
              selectedPlan={this.selectedPlan}
              selected={this.state.globalSelected}
              annualMode={this.state.annualMode}
              price={
                this.state.annualMode
                  ? this.state.globalPrice
                  : this.state.globalPrice / 12
              }
              plan="Global"
            />
          </Grid>
          <Grid item>
            <SubscriptionCard
              selectedPlan={this.selectedPlan}
              selected={this.state.universeSelected}
              annualMode={this.state.annualMode}
              price={
                this.state.annualMode
                  ? this.state.universePrice
                  : this.state.universePrice / 12
              }
              plan="Universe"
            />
          </Grid>
        </Grid>
        <p><span className="show-comparison">Show me the full comparison table</span> <img alt="" src={comparisonIcon}/></p>
      </div>
    );
  }
}

export default SubscriptionPage;
