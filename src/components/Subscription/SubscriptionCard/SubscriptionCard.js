import React, { Component } from "react";
import "./SubscriptionCard.css";
import validIcon from "../../../assets/svg/valid.svg";

class SubscriptionCard extends Component {
  constructor(props) {
    super(props);

    this.state = {
      selected: false
    };

    this.handleSelect = this.handleSelect.bind(this);

    this.classes = {
      universe: {
        container: "SubscriptionCard-Universe",
        title: "_title-universe",
        priceContainer: "_price-container-universe",
        priceDescription: "_price-description-universe",
        detail: "_detail-universe",
        button: "_button-universe"
      },
      global: {
        container: "SubscriptionCard-Global",
        title: "_title-global",
        priceContainer: "_price-container-global",
        priceDescription: "_price-description-global",
        detail: "_detail-global",
        button: "_button-global"
      }
    };

    this.details = {
      universe: {
        travelDuration: "180",
        medical: "3.000.000",
        abroad: "10.000",
        travelAssit: "2.500",
        coverDuration: "1"
      },
      global: {
        travelDuration: "90",
        medical: "1.000.000",
        abroad: "5.000",
        travelAssit: "1.000",
        coverDuration: "1"
      }
    };
  }

  handleSelect() {
    this.props.selectedPlan(this.props.plan);
  }

  formatCurrency(n) {
    return n.toFixed(2).replace(/./g, (c, i, a) => {
      return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
    }).replace(/./g, (c,i,a)=>{
      if (c === "," || c === "."){
        return c === "," ? "." : ",";
      }
      return c;
    });

  }

  render() {
    const style =
      this.props.plan === "Global"
        ? this.classes.global
        : this.classes.universe;
    const details =
      this.props.plan === "Global"
        ? this.details.global
        : this.details.universe;
    const billingPeriod = this.props.annualMode ? "yearly" : "monthly";
    return (
      <div className={style.container}>
        <div className={style.title}> {this.props.plan}</div>
        <div className={style.priceContainer}>
          <div className="_price">
            {this.formatCurrency(this.props.price)} <sup>€</sup>{" "}
          </div>
          <div className={style.priceDescription}>
            {billingPeriod} incl. taxes
          </div>
        </div>
        <div className={style.detail}>
          Maximum duration travel <p className="text-style-1">of</p>{" "}
          {details.travelDuration} days
        </div>
        <div className={style.detail}>
          Medical expenses reimbursement <p className="text-style-1">up to </p>
          {details.medical} €
        </div>
        <div className={style.detail}>
          Personal assistance abroad <p className="text-style-1">up to </p>
          {details.abroad} €
        </div>
        <div className={style.detail}>
          Travel assistance abroad <p className="text-style-1">up to </p>{" "}
          {details.travelAssit} €
          <br />
          <p className="text-style-1">per insured per travel</p>
        </div>
        <div className={style.detail}>
          Coverage duration: {details.coverDuration} year
        </div>
        <div>
          <button
            onClick={this.handleSelect}
            hidden={this.props.price === 0}
            className={style.button}
          >
            {this.props.selected ? 
              <img alt="" className="imgValid" src={validIcon}/> : null}
            {this.props.selected ? 
             "Plan selected" : "Choose this plan"}
          </button>
        </div>
      </div>
    );
  }
}

export default SubscriptionCard;
