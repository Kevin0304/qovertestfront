import React, { Component } from "react";
import "./QuoteForm.css";
import QuoteInput from "../QuoteInput";
import QuoteSubmitBtn from "../QuoteSubmitBtn";

class QuoteForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      age: "",
      car: "",
      price: "",
      validAge: false,
      validPrice: false,
      validCar: false,
      validForm: false,
      showAgeError: false,
      showPriceError: false,
      showCarError: false
    };
    this.carOptions = ["AUDI", "BMW", "PORSCHE"];
    this.error = {
      age: "Sorry! the driver is too young",
      price: "Sorry! The price of the car is too low",
      car: "Sorry! We can not accept this particular risk"
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleKeyUp = this.handleKeyUp.bind(this);
  }

  handleSubmit() {
    // console.log("handle sumit QuoteForm");
    this.setState(
      {
        showAgeError: parseInt(this.state.age) < 18,
        showCarError:
          parseFloat(this.state.age) < 25 && this.state.car === "PORSCHE",
        showPriceError: parseFloat(this.state.price) < 5000
      },
      this.navigateToSubscription
    );
  }

  navigateToSubscription() {
    if (
      !this.state.showAgeError &&
      !this.state.showCarError &&
      !this.state.showPriceError
    ) {
      return this.props.gotoSubscriptions(this.state.car, this.state.price);
    } else {
      return;
    }
  }

  handleKeyUp(event) {
    if (event.keyCode === 13 && this.state.validForm)
      return this.handleSubmit();
    else return;
  }

  handleChange(event) {
    const name = event.target.name;
    let value = event.target.value;
    if ((name === "price" || name === "age") && value.trim() !== "") {
      value = value.replace(/[^\d.-]/g, "");
      if (name === "age") value = parseInt(value) + "";
    }

    return this.setState({ [name]: value }, () => {
      this.validateField(name, value);
    });
  }

  validateField(fieldName, value) {
    let validAge = this.state.validAge;
    let validCar = this.state.validCar;
    let validPrice = this.state.validPrice;

    switch (fieldName) {
      case "age":
        validAge = Number.isInteger(parseInt(value));
        break;
      case "car":
        validCar = value.length > 0;
        break;
      case "price":
        validPrice = !isNaN(parseFloat(value));
        break;
      default:
        break;
    }
    return this.setState(
      {
        validAge: validAge,
        validCar: validCar,
        validPrice: validPrice
      },
      this.validateForm
    );
  }

  validateForm() {
    return this.setState({
      validForm:
        this.state.validCar && this.state.validAge && this.state.validPrice
    });
  }

  render() {
    return (
      <div className="Form-Container">
        <QuoteInput
          handleChange={this.handleChange}
          pressEnter={this.handleKeyUp}
          name="age"
          type="text"
          value={this.state.age}
          label="Age of the driver"
          error={this.state.showAgeError}
          errorMessage={this.error.age}
        />
        <br />
        <QuoteInput
          handleChange={this.handleChange}
          pressEnter={this.handleKeyUp}
          name="car"
          carOptions={this.carOptions}
          value={this.state.car}
          label="Car"
          error={this.state.showCarError}
          errorMessage={this.error.car}
        />
        <br />
        <QuoteInput
          handleChange={this.handleChange}
          pressEnter={this.handleKeyUp}
          name="price"
          type="text"
          value={this.state.price}
          label="Purchase Price"
          error={this.state.showPriceError}
          errorMessage={this.error.price}
        />
        <br />
        <QuoteSubmitBtn
          text="Get a price"
          click={this.handleSubmit}
          enable={this.state.validForm}
        />
      </div>
    );
  }
}

export default QuoteForm;
