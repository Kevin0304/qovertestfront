import React, { Component } from "react";
import "./QuoteInput.css";
import { InputLabel, TextField, Grid, MenuItem } from "@material-ui/core";

class QuoteInput extends Component {
  
  render() {
    return (
      <div className="input-container">
        <Grid
          alignItems="center"
          container
          align="center"
          justify="center"
          item
          xs={12}
        >
          <Grid className="label-grid" item xs={4}>
            <InputLabel htmlFor={this.props.name}>
              {this.props.label}
            </InputLabel>
          </Grid>
          <Grid className="input-grid" item xs={4}>
            {this.props.name === "car" ? (
              <TextField
                select
                className="dropdown"
                error={this.props.error}
                name={this.props.name}
                value={this.props.value}
                type={this.props.type}
                margin="normal"
                variant="outlined"
                onKeyDown={this.props.pressEnter}
                onChange={this.props.handleChange}
                
              >
                {this.props.carOptions
                  ? this.props.carOptions.map((option, i) => (
                      <MenuItem key={i} value={option}>
                        {option}
                      </MenuItem>
                    ))
                  : null}
              </TextField>
            ) : (
              <TextField
                className="number"
                onKeyDown={this.props.pressEnter}
                error={this.props.error}
                min="1"
                name={this.props.name}
                value={this.props.value}
                type={this.props.type}
                margin="normal"
                variant="outlined"
                onChange={this.props.handleChange}
              />
            )}
            {this.props.name === "price" ? (
              <div className={this.props.error ? "label2-error" : "label2"}>
                €
              </div>
            ) : null}
            <div className="error-message">
              {this.props.error ? this.props.errorMessage : ""}
            </div>
          </Grid>
        </Grid>
      </div>
    );
  }
}

export default QuoteInput;
