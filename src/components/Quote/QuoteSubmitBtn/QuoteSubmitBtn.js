import React, { Component } from "react";
import "./QuoteSubmitBtn.css";

class QuoteSubmitBtn extends Component {
  render() {
    return (
      <button
        onClick = {this.props.click}
        disabled={!this.props.enable}
        className={!this.props.enable ? "Disabled-Q-Submit" : "Q-Submit-Btn"}
      >
        {this.props.text}
      </button>
    );
  }
}

export default QuoteSubmitBtn;
