import React, { Component } from "react";
import "./QuoteRequest.css";
import QuoteForm from "../QuoteForm/QuoteForm";
import Header from "../../Header";
import Footer from "../../Footer";

class QuoteRequest extends Component {
  constructor(props) {
    super(props);

    this.gotoSubscriptions = this.gotoSubscriptions.bind(this);
    // this.calculatePrice = this.calculatePrice.bind(this);
  }

  gotoSubscriptions(car, price) {
    if (car && price) {
      this.calculatePriceAndShowPlans(car, price);
    }
  }

  calculatePriceAndShowPlans(car, p) {
    let globalPrice;
    let universePrice;
    let price = parseFloat(p);
    // console.log("I AM HERE IN SWITCH");
    switch (car) {
      case "PORSCHE":
         globalPrice = 500;
         universePrice = globalPrice + (price / 100) * 0.7;
        return this.props.history.push(
          `/subscription/ ${globalPrice}/${universePrice}`
        );

      case "AUDI":
         globalPrice = 250;
         universePrice = globalPrice + (price / 100) * 0.3;
        return this.props.history.push(
          `/subscription/ ${globalPrice}/${universePrice}`
        );

      case "BMW":
         globalPrice = 150;
         universePrice = globalPrice + (price / 100) * 0.4;
        return this.props.history.push(
          `/subscription/ ${globalPrice}/${universePrice}`
        );
      default:
        return;
    }
  }

  render() {
    return (
      <div className="Mask2">
        <Header/>
        <div className="container2">
          <QuoteForm gotoSubscriptions={this.gotoSubscriptions} />
        </div>
        <Footer/>
      </div>
    );
  }
}

export default QuoteRequest;
