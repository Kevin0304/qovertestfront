import React, { Component } from 'react';
import './Header.css';
import Api from '../../services/Api'

class Header extends Component {
  constructor(props){
    super(props)

    this.handleLogOut = this.handleLogOut.bind(this);
  }

  handleLogOut(){
    Api.logOut();
    window.location.reload();
    
  }

  render() {
    return (
      <div className="Rectangle-2">
          <div className="qoverme"> {"< QOVER.ME"} </div>
          <div onClick = {this.handleLogOut} hidden={!localStorage.getItem("sessionToken")} className="logout"><img alt="" style={{width:"50%"}} src="https://img.icons8.com/ultraviolet/40/000000/shutdown.png"/><br></br> LOGOUT</div>
      </div>
    );
  }

}

export default Header;