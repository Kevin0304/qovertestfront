import React, { Component } from 'react';
import './Footer.css';

class Footer extends Component {

  render() {
    return (
      <div className="Footer">
        © Qover 2019
      </div>
    );
  }
}

export default Footer;
