import React, { Component } from "react";
import "./App.css";

class App extends Component {
  componentWillMount() {
    return !localStorage.getItem("sessionToken")
      ? this.props.history.push("/auth")
      : this.props.history.push("/quote-request");
  }

  render() {
    return <div className="App" />;
  }
}

export default App;
