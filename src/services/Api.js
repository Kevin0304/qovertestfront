import axios from "axios";
const API_ENDPOINT = "http://localhost:3978/api/";

class ApiService {
  constructor() {
    this.authHeader = { authorization: this.getLocalSessionToken() };
  }

  refreshHeader() {
    this.authHeader = { authorization: this.getLocalSessionToken() };
  }

  getLocalSessionToken() {
    sessionStorage.getItem("sessionToken");
  }

  saveLocalSessionToken(token) {
    return localStorage.setItem("sessionToken", token);
  }

  deleteLocalSessionToken() {
    return localStorage.removeItem("sessionToken");
  }

  //   addUser(user) {
  //     return axios.post(API_ENDPOINT + "userclient/create", user, {
  //       headers: this.authHeader
  //     });
  //   }

  updateUser(user) {
    return axios.post(API_ENDPOINT + "userclient/update", user, {
      headers: this.authHeader
    });
  }

  logIn(user) {
    let headers = {
      email: `${user.email}`,
      password: `${user.password}`
    };

    return axios.post(API_ENDPOINT + "gettoken", user, { headers: headers });
  }

  logOut(user) {
    this.deleteLocalSessionToken();
    return axios.post(API_ENDPOINT + "userclient/logout", user, {
      headers: this.authHeader
    });
  }
}
export default new ApiService();
