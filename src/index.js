import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './index.css';
import AuthPage from './components/Auth/AuthPage';
import { Route, BrowserRouter as Router } from 'react-router-dom'
import QuoteRequest from './components/Quote/QuoteRequest';
import SubscriptionPage from './components/Subscription/SubscriptionPage';

const routing = (
  <Router>
      <Route path="/auth" component={AuthPage} />
      <Route path="/quote-request" component={QuoteRequest} />
      <Route path="/subscription/:globalPrice/:universePrice" component={SubscriptionPage} />
      <Route component={App} />
  </Router>
)
ReactDOM.render(routing, document.getElementById('root'))
